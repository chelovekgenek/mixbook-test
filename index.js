'use strict'

const data = require('./data.json')

let iterator = 0

function isWord(str) {
  iterator++;
  const words = ['pop', 'popcorn', 'non']
  if (words.find(item => item.toUpperCase() === str.toUpperCase())) console.log(str)
}

data.forEach(item => parseWords(item.relations, [item.id]))
console.log('iterator', iterator)

function parseWords(relations, passed, limit) {
  isWord(buildStr(passed))

  const uniques = getUniques(relations, passed)
  if (uniques)
    uniques.forEach(relation => 
      parseWords(getRelations(relation), passed.concat(relation), limit)
    )
  else return 
}

function buildStr(passed) {
  return passed.map(item => getLetter(item)).join('')
}

function getUniques(relations, passed) {
  return relations.filter(relation => !passed.includes(relation))
}

function getObj(id) {
  return data.find(item => item.id === id)
}
function getLetter(id) {
  return getObj(id).letter
}
function getRelations(id) {
  return getObj(id).relations
}

